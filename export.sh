#!/bin/env bash

usage() {
  echo "Usage: bash $0 [-r epics_root] [-h] destination"
  echo "   -r epics_root: Install location for epics. Default /epics"
  echo "              -h: This help message"
  echo "     destination: location to install the output files"
}

root="/epics"
while getopts "r:h" opt; do
  case $opt in
    r)
      root="$OPTARG"
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
      usage
      exit 1
      ;;
  esac
done

shift $((OPTIND - 1))

if [ $# -ge 1 ]; then
  dest="$1"
else
  dest=/var/www/test
fi

function die() {
  if [ $# -ge 1 ]; then
    echo -e "Error: $1"
  else
    echo -e "Error: Export script failed."
  fi
  exit 1
}

python3 generate_nfs_package_info.py --root "$root" || die "Package info generation failed"

python3 freeze.py || die "Flask-Frozen failed"

[ -d "$dest" ] || mkdir -p "$dest" || die "Target directory does not exist, and cannot be created."

rsync -a --delete public/ "$dest"/nfs-package-overview || die "rsync failed"
