$(document).ready(function () {

  $('#nfs_packages_table').DataTable(
    {
      "paging": false,
      "info": false,
      "order": [[0, 'asc'], [1, 'asc']],
      "columnDefs": [
        {
          "visible": false,
          "targets": [0,]
        },
        {
          "targets": [0, 1, ],
          "orderable": false
        }
      ],
      "rowGroup": {
        dataSrc: [
          0,
        ]
      }

    }
  );

});
