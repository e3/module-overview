import datetime
import json
from pathlib import Path
from flask import Flask, g, render_template, jsonify


app = Flask(__name__)
app.config.from_mapping(
    FREEZER_DESTINATION="../public",
    FREEZER_BASE_URL="http://e3.pages.esss.lu.se/module-overview/",
    FREEZER_DEFAULT_MIMETYPE="application/json",
    NFS_PACKAGES_INFO=Path(app.root_path) / "static/json/nfs_package_info_table.json",
)


def nfs_packages_info():
    info = getattr(g, "_nfs_packages_info_table", None)
    if info is None:
        with open(app.config["NFS_PACKAGES_INFO"]) as f:
            info = g._nfs_packages_info = json.load(f)
    return info


@app.context_processor
def current_time():
    return dict(current_time=datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M"))


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/nfs_tree/")
def list_e3_modules():
    return render_template("nfs_tree.html")


@app.route("/nfs_packages/")
def list_nfs_packages():
    info = nfs_packages_info()
    return render_template("nfs_table.html", packages=info)


@app.route("/nfs_dependency/")
def list_dependencies():
    return render_template("nfs_dependency.html")
