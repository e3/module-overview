import re
import sys
from functools import total_ordering


@total_ordering
class SimpleSemver:
    def __init__(self, stringver):
        self.stringver = stringver.strip()

        regex = r"(?:(\d+)(?:\.(\d+)(?:\.(\d+)(?:\.(\d+)(?:\+(\d+))?)?)?)?)?(.*)"

        match = re.match(regex, self.stringver)
        if match:
            self.major = SimpleSemver.parse_arg(match.group(1), int, 0)
            self.minor = SimpleSemver.parse_arg(match.group(2), int, 0)
            self.patch = SimpleSemver.parse_arg(match.group(3), int, 0)
            self.patch2 = SimpleSemver.parse_arg(match.group(4), int, 0)
            self.build = SimpleSemver.parse_arg(match.group(5), int, 0)
            self.testv = SimpleSemver.parse_arg(match.group(6), str, "")
        else:
            raise Exception("This should not be possible")

    @staticmethod
    def parse_arg(arg, callback, default):
        return default if arg is None else callback(arg)

    def __str__(self):
        return f"{self.major}.{self.minor}.{self.patch}.{self.patch2}+{self.build}-{self.testv}"

    def __eq__(self, other):
        return self.stringver == other.stringver

    def __lt__(self, other):
        if self.testv and other.testv:
            return self.testv < other.testv
        elif self.testv:
            return True
        elif other.testv:
            return False
        else:
            return (self.major, self.minor, self.patch, self.patch2, self.build,) < (
                other.major,
                other.minor,
                other.patch,
                other.patch2,
                other.build,
            )


if __name__ == "__main__":
    svs = []
    for arg in sys.argv[1:]:
        svs.append(SimpleSemver(arg))

    for sv in sorted(svs):
        print(str(sv))
